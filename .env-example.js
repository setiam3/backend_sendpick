
DATABASE_URL="postgresql://<yourusername>:<yourpassword>@<dbhost>:<dbPORT>/<databasename>?schema=<yourschema>"
DEBUG="*"

JWT_KEY="your_jwt_key"
JWT_EXPIRED="your_jwt_expired_time"

PORT=3000
