import { PrismaClient } from '@prisma/client';
import { cloneDeep } from 'lodash';
import * as bcrypt from 'bcrypt';
import { randomUUID } from 'crypto';

export const roles = [
  {
    id: 1,
    name: 'admin',
  },
  {
    id: 2,
    name: 'user',
  },
  {
    id: 3,
    name: 'operational',
  },
  {
    id: 4,
    name: 'finance',
  },
  {
    id: 5,
    name: 'customer',
  },
  {
    id: 6,
    name: 'vendor',
  },
];

export const permissions = [
  {
    id: 1,
    role_id: 1,
    action: 'manage',
    subject: 'all',
  },
  {
    id: 2,
    role_id: 2,
    action: 'read',
    subject: 'User',
  },
  {
    id: 3,
    role_id: 2,
    action: 'manage',
    subject: 'User',
    conditions: { user_id: '{{ id }}' },
  },
];

export const users = [
  {
    id: randomUUID(),
    first_name: 'Jhon',
    last_name: 'Doe',
    role_id: 1,
    email: 'admin@gmail.com',
    // password: '$2a$12$UYb8jfT3g6yhR639ClJ9.epQ4kia94iV9aJrYc1R5BNzI5sz1XND2',
    password: bcrypt.hashSync('password', 10),
  },
  {
    id: randomUUID(),
    first_name: 'Sean',
    last_name: 'Dave',
    role_id: 2,
    email: 'user@gmail.com',
    // password: '$2a$12$UYb8jfT3g6yhR639ClJ9.epQ4kia94iV9aJrYc1R5BNzI5sz1XND2',
    password: bcrypt.hashSync('password', 10),
  },
];

export const account_types = [
  {
    id: randomUUID(),
    name: 'KAS',
  },
  {
    id: randomUUID(),
    name: 'ASET LAINNYA',
  },
  {
    id: randomUUID(),
    name: 'ASET LANCAR LAINNYA',
  },
  {
    id: randomUUID(),
    name: 'PIUTANG USAHA BERSIH',
  },
  {
    id: randomUUID(),
    name: 'PIUTANG LAIN - LAIN',
  },
  {
    id: randomUUID(),
    name: 'INVENTORY',
  },
  {
    id: randomUUID(),
    name: 'HARTA TETAP BERWUJUD -  BERSIH',
  },
  {
    id: randomUUID(),
    name: 'AKUMULASI PENYUSUTAN',
  },
  {
    id: randomUUID(),
    name: 'HUTANG USAHA',
  },
  {
    id: randomUUID(),
    name: 'HUTANG LAIN-LAIN',
  },
  {
    id: randomUUID(),
    name: 'HUTANG JANGKA PANJANG',
  },
  {
    id: randomUUID(),
    name: 'MODAL SAHAM',
  },
  {
    id: randomUUID(),
    name: 'LABA DITAHAN',
  },
  {
    id: randomUUID(),
    name: 'LABA TAHUN BERJALAN',
  },
  {
    id: randomUUID(),
    name: 'PENDAPATAN',
  },
  {
    id: randomUUID(),
    name: 'BEBAN',
  },
  {
    id: randomUUID(),
    name: 'PENDAPATAN LAINNYA',
  },
  {
    id: randomUUID(),
    name: 'PIUTANG TRANSIT ACCOUNT',
  },
  {
    id: randomUUID(),
    name: 'PAJAK DIBAYAR DIMUKA',
  },
  {
    id: randomUUID(),
    name: 'BIAYA DIBAYAR DIMUKA',
  },
  {
    id: randomUUID(),
    name: 'UANG MUKA PEMBELIAN',
  },
  {
    id: randomUUID(),
    name: 'PIUTANG PEMEGANG SAHAM',
  },
  {
    id: randomUUID(),
    name: 'HARTA TETAP TIDAK BERWUJUD -  BERSIH',
  },
  {
    id: randomUUID(),
    name: 'HARTA KERETA TEMPEL - TAX AMNESTY',
  },
  {
    id: randomUUID(),
    name: 'KEWAJIBAN LANCAR',
  },
  {
    id: randomUUID(),
    name: 'HUTANG PAJAK',
  },
  {
    id: randomUUID(),
    name: 'UANG MUKA PENDAPATAN',
  },
  {
    id: randomUUID(),
    name: 'BIAYA YMH DIBAYAR',
  },
  {
    id: randomUUID(),
    name: 'HUTANG TRANSITORIS ACCOUNT',
  },
  {
    id: randomUUID(),
    name: 'JOB COSTING ACCRUAL',
  },
  {
    id: randomUUID(),
    name: 'HUTANG BANK',
  },
  {
    id: randomUUID(),
    name: 'HUTANG SEWA',
  },
  {
    id: randomUUID(),
    name: 'HUTANG PEMEGANG SAHAM',
  },
  {
    id: randomUUID(),
    name: 'CADANGAN PESANGON',
  },
  {
    id: randomUUID(),
    name: 'HUTANG  NON BANK',
  },
  {
    id: randomUUID(),
    name: 'LABA RUGI  DITAHAN TAX AMNESTY',
  },
];

const prisma = new PrismaClient();
async function main() {
  for await (const role of roles) {
    const roleAttrs = cloneDeep(role);
    delete roleAttrs.id;
    await prisma.role.upsert({
      where: {
        id: role.id,
      },
      create: roleAttrs,
      update: roleAttrs,
    });
  }
  for await (const permission of permissions) {
    const permissionAttrs = cloneDeep(permission);
    delete permissionAttrs.id;
    await prisma.permission.upsert({
      where: {
        id: permission.id,
      },
      create: permissionAttrs,
      update: permissionAttrs,
    });
  }
  for await (const user of users) {
    const userAttrs = cloneDeep(user);
    delete userAttrs.id;
    await prisma.user.upsert({
      where: {
        id: user.id,
      },
      create: userAttrs,
      update: userAttrs,
    });
  }

  for await (const account_type of account_types) {
    const account_typeAttrs = cloneDeep(account_type);
    delete account_typeAttrs.id;
    await prisma.accountType.upsert({
      where: {
        id: account_type.id,
      },
      create: account_typeAttrs,
      update: account_typeAttrs,
    });
  }
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (error) => {
    console.log(error);
    await prisma.$disconnect();
  });
