###################
# BUILD FOR LOCAL DEVELOPMENT
###################
FROM node:20.13.1-alpine3.19 AS development
# Create app directory
WORKDIR /usr/src/app
# Copy application dependency manifests to the container image.
COPY --chown=node:node package*.json ./
# Install app dependencies using the `npm ci` command instead of `npm install`
RUN npm install
# Bundle app source
COPY --chown=node:node . .
# Use the node user from the image (instead of the root user)
USER node
###################
# BUILD FOR PRODUCTION
###################
FROM node:20.13.1-alpine3.19 AS build
WORKDIR /usr/src/app
# Copy application dependency manifests to the container image.
COPY --chown=node:node package*.json ./
# Copy node_modules from development stage
COPY --chown=node:node --from=development /usr/src/app/node_modules ./node_modules
# Bundle app source
COPY --chown=node:node . .
# Set environment variables required for Prisma
ARG DATABASE_URL
ENV DATABASE_URL=${DATABASE_URL}
# Run Prisma to push schema to the database
RUN npx prisma db push --accept-data-loss --force-reset
# Seed the database
RUN npm run seed
# Run the build command which creates the production bundle
RUN npm run build
# Set NODE_ENV environment variable
ENV NODE_ENV production
# Running `npm ci` removes the existing node_modules directory and passing in --only=production ensures that only the production dependencies are installed. This ensures that the node_modules directory is as optimized as possible
RUN npm install --only=production && npm cache clean --force
USER node
###################
# PRODUCTION
###################
FROM node:20.13.1-alpine3.19 AS production
# Create app directory
WORKDIR /usr/src/app
# Copy the bundled code from the build stage to the production image
COPY --chown=node:node --from=build /usr/src/app/node_modules ./node_modules
COPY --chown=node:node --from=build /usr/src/app/dist ./dist
# Expose the port the app runs on
EXPOSE 3000
# Start the server using the production build
CMD [ "node", "dist/main.js" ]
