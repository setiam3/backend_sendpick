import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class DeleteManyCountryDto {
  @ApiProperty()
  @IsNotEmpty()
  country_ids: Array<string>;
}
