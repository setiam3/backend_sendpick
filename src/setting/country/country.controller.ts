import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
  UseGuards,
  Put,
} from '@nestjs/common';
import { CountryService } from './country.service';
import { CreateCountryDto } from './dto/create-country.dto';
import { UpdateCountryDto } from './dto/update-country.dto';
import { FindCountryDto } from './dto/find-country.dto';
import { DeleteManyCountryDto } from './dto/delete-many-country.dto';
import { ApiTags } from '@nestjs/swagger';
import { AbilitiesGuard } from '../../core/casl/abilities.guard';

@Controller('countries')
@ApiTags('Country')
@UseGuards(AbilitiesGuard)
export class CountryController {
  constructor(private readonly countryService: CountryService) {}

  @Post()
  create(@Body() createCountryDto: CreateCountryDto) {
    return this.countryService.create(createCountryDto);
  }

  @Get()
  findAll(@Query() findAllCountryDto: FindCountryDto) {
    return this.countryService.findAll(findAllCountryDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.countryService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateCountryDto: UpdateCountryDto) {
    return this.countryService.update(id, updateCountryDto);
  }

  @Delete()
  removeMany(@Query() country_ids: DeleteManyCountryDto) {
    return this.countryService.removeMany(country_ids);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.countryService.remove(id);
  }
}
