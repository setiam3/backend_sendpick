import { Injectable } from '@nestjs/common';
import { CreateCountryDto } from './dto/create-country.dto';
import { UpdateCountryDto } from './dto/update-country.dto';
import { DeleteManyCountryDto } from './dto/delete-many-country.dto';
import { FindCountryDto } from './dto/find-country.dto';
import { PrismaService } from '../../core/prisma/prisma.service';

import {
  PaginateFunction,
  paginator,
} from '../../core/prisma/prisma-pagination';

@Injectable()
export class CountryService {
  constructor(private prisma: PrismaService) {}

  create(CreateCountryDto: CreateCountryDto) {
    return this.prisma.country.create({
      data: CreateCountryDto,
    });
  }

  findAll(FindCountryDto: FindCountryDto) {
    const paginate: PaginateFunction = paginator({
      perPage: FindCountryDto.per_page,
      page: FindCountryDto.page,
    });
    const params = FindCountryDto;
    return paginate(
      this.prisma.country,
      {
        where: {
          name: { contains: FindCountryDto.filter_name, mode: 'insensitive' },
        },
        orderBy: { created_at: 'desc' },
      },
      {},
      params,
    );
  }

  findOne(id: string) {
    try {
      return this.prisma.country.findFirstOrThrow({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }

  update(id: string, UpdateCountryDto: UpdateCountryDto) {
    return this.prisma.country.update({
      where: {
        id,
      },
      data: {
        ...UpdateCountryDto,
      },
    });
  }

  remove(id: string) {
    try {
      return this.prisma.country.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  removeMany(DeleteManyCountryDto: DeleteManyCountryDto) {
    try {
      return this.prisma.country.deleteMany({
        where: {
          id: {
            in: DeleteManyCountryDto.country_ids,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
