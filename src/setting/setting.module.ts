import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { MenuModule } from './menu/menu.module';
import { AreaModule } from './area/area.module';
import { CountryModule } from './country/country.module';
import { ProvinceModule } from './province/province.module';
import { CityModule } from './city/city.module';
import { UnitModule } from './unit/unit.module';
import { BankModule } from './bank/bank.module';
import { AccountModule } from './finance/account/account.module';
import { AccountTypeModule } from './finance/account-type/account-type.module';
import { VendorJobStatusModule } from './vendor/vendor-job-status/vendor-job-status.module';
import { VendorTypeModule } from './vendor/vendor-type/vendor-type.module';

@Module({
  imports: [
    UserModule,
    CityModule,
    MenuModule,
    AreaModule,
    CountryModule,
    ProvinceModule,
    UnitModule,
    BankModule,
    AccountModule,
    AccountTypeModule,
    VendorJobStatusModule,
    VendorTypeModule,
  ],
})
export class SettingModule {}
