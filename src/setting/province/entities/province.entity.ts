export class Province {
  id: string;
  code: string;
  name: string;
  country_id: string;
}
