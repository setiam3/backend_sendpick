import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class DeleteManyProvinceDto {
  @ApiProperty()
  @IsNotEmpty()
  province_ids: Array<string>;
}
