import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsString, IsInt, IsOptional, IsPositive } from 'class-validator';

export class FindProvinceDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  filter_name?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  filter_code?: string;

  /** Show products in this page
   * @example 1
   */
  @ApiProperty()
  @IsOptional()
  @Type(() => Number)
  @IsInt()
  @IsPositive()
  page?: number;

  /** Show this amount of products per page
   * @example 10
   */
  @ApiProperty()
  @IsOptional()
  @Type(() => Number)
  @IsInt()
  @IsPositive()
  per_page?: number;
}
