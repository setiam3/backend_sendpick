import { Injectable } from '@nestjs/common';
import { CreateProvinceDto } from './dto/create-province.dto';
import { UpdateProvinceDto } from './dto/update-province.dto';
import { FindProvinceDto } from './dto/find-province.dto';
import { DeleteManyProvinceDto } from './dto/delete-many-province.dto';
import { PrismaService } from '../../core/prisma/prisma.service';

import {
  PaginateFunction,
  paginator,
} from '../../core/prisma/prisma-pagination';

@Injectable()
export class ProvinceService {
  constructor(private prisma: PrismaService) {}

  create(CreateProvinceDto: CreateProvinceDto) {
    return this.prisma.province.create({
      data: CreateProvinceDto,
    });
  }

  findAll(FindProvinceDto: FindProvinceDto) {
    const paginate: PaginateFunction = paginator({
      perPage: FindProvinceDto.per_page,
      page: FindProvinceDto.page,
    });
    const params = FindProvinceDto;
    return paginate(
      this.prisma.province,
      {
        where: {
          name: { contains: FindProvinceDto.filter_name, mode: 'insensitive' },
          code: { contains: FindProvinceDto.filter_code, mode: 'insensitive' },
        },
        orderBy: { created_at: 'desc' },
        include: {
          country: {
            select: {
              id: true,
              name: true,
            },
          },
        },
      },
      {},
      params,
    );
  }

  findOne(id: string) {
    try {
      return this.prisma.province.findFirstOrThrow({
        where: {
          id,
        },
        include: {
          country: {
            select: {
              id: true,
              name: true,
            },
          },
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }

  update(id: string, UpdateProvinceDto: UpdateProvinceDto) {
    return this.prisma.province.update({
      where: {
        id,
      },
      data: {
        ...UpdateProvinceDto,
      },
      include: {
        country: {
          select: {
            id: true,
            name: true,
          },
        },
      },
    });
  }

  remove(id: string) {
    try {
      return this.prisma.province.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  removeMany(DeleteManyProvinceDto: DeleteManyProvinceDto) {
    try {
      return this.prisma.province.deleteMany({
        where: {
          id: {
            in: DeleteManyProvinceDto.province_ids,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
