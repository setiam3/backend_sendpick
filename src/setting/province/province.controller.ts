import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Query,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ProvinceService } from './province.service';
import { CreateProvinceDto } from './dto/create-province.dto';
import { UpdateProvinceDto } from './dto/update-province.dto';
import { FindProvinceDto } from './dto/find-province.dto';
import { DeleteManyProvinceDto } from './dto/delete-many-province.dto';
import { AbilitiesGuard } from '../../core/casl/abilities.guard';
import { ApiTags } from '@nestjs/swagger';

@Controller('provinces')
@ApiTags('Province')
@UseGuards(AbilitiesGuard)
export class ProvinceController {
  constructor(private readonly provinceService: ProvinceService) {}

  @Post()
  create(@Body() createProvinceDto: CreateProvinceDto) {
    return this.provinceService.create(createProvinceDto);
  }

  @Get()
  findAll(@Query() findAllProvinceDto: FindProvinceDto) {
    return this.provinceService.findAll(findAllProvinceDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.provinceService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateProvinceDto: UpdateProvinceDto,
  ) {
    return this.provinceService.update(id, updateProvinceDto);
  }

  @Delete()
  removeMany(@Query() province_ids: DeleteManyProvinceDto) {
    return this.provinceService.removeMany(province_ids);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.provinceService.remove(id);
  }
}
