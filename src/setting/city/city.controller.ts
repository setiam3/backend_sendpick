import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Query,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CityService } from './city.service';
import { CreateCityDto } from './dto/create-city.dto';
import { UpdateCityDto } from './dto/update-city.dto';
import { FindCityDto } from './dto/find-city.dto';
import { DeleteManyCityDto } from './dto/delete-many-city.dto';
import { AbilitiesGuard } from '../../core/casl/abilities.guard';
import { ApiTags } from '@nestjs/swagger';

@Controller('cities')
@ApiTags('City')
@UseGuards(AbilitiesGuard)
export class CityController {
  constructor(private readonly cityService: CityService) {}

  @Post()
  create(@Body() createCityDto: CreateCityDto) {
    return this.cityService.create(createCityDto);
  }

  @Get()
  findAll(@Query() findAllCityDto: FindCityDto) {
    return this.cityService.findAll(findAllCityDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.cityService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateCityDto: UpdateCityDto) {
    return this.cityService.update(id, updateCityDto);
  }

  @Delete()
  removeMany(@Query() city_ids: DeleteManyCityDto) {
    return this.cityService.removeMany(city_ids);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cityService.remove(id);
  }
}
