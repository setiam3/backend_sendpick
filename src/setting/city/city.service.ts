import { Injectable } from '@nestjs/common';
import { CreateCityDto } from './dto/create-city.dto';
import { UpdateCityDto } from './dto/update-city.dto';
import { FindCityDto } from './dto/find-city.dto';
import { DeleteManyCityDto } from './dto/delete-many-city.dto';
import { PrismaService } from '../../core/prisma/prisma.service';

import {
  PaginateFunction,
  paginator,
} from '../../core/prisma/prisma-pagination';

@Injectable()
export class CityService {
  constructor(private prisma: PrismaService) {}

  create(CreateCityDto: CreateCityDto) {
    return this.prisma.city.create({
      data: CreateCityDto,
    });
  }

  findAll(FindCityDto: FindCityDto) {
    const paginate: PaginateFunction = paginator({
      perPage: FindCityDto.per_page,
      page: FindCityDto.page,
    });
    const params = FindCityDto;
    return paginate(
      this.prisma.city,
      {
        where: {
          name: { contains: FindCityDto.filter_name, mode: 'insensitive' },
          code: { contains: FindCityDto.filter_code, mode: 'insensitive' },
        },
        orderBy: { created_at: 'desc' },
        include: {
          country: {
            select: {
              id: true,
              name: true,
            },
          },
          province: {
            select: {
              id: true,
              name: true,
            },
          },
        },
      },
      {},
      params,
    );
  }

  findOne(id: string) {
    try {
      return this.prisma.city.findFirstOrThrow({
        where: {
          id,
        },
        include: {
          country: {
            select: {
              id: true,
              name: true,
            },
          },
          province: {
            select: {
              id: true,
              name: true,
            },
          },
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }

  update(id: string, UpdateCityDto: UpdateCityDto) {
    return this.prisma.city.update({
      where: {
        id,
      },
      data: {
        ...UpdateCityDto,
      },
      include: {
        country: {
          select: {
            id: true,
            name: true,
          },
        },
        province: {
          select: {
            id: true,
            name: true,
          },
        },
      },
    });
  }

  remove(id: string) {
    try {
      return this.prisma.city.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  removeMany(DeleteManyCityDto: DeleteManyCityDto) {
    try {
      return this.prisma.city.deleteMany({
        where: {
          id: {
            in: DeleteManyCityDto.city_ids,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
