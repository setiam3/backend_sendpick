export class City {
  id: string;
  code: string;
  name: string;
  province_id: string;
  country_id: string;
}
