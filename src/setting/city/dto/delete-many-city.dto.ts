import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class DeleteManyCityDto {
  @ApiProperty()
  @IsNotEmpty()
  city_ids: Array<string>;
}
