import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class DeleteManyUnitDto {
  @ApiProperty()
  @IsNotEmpty()
  unit_ids: Array<string>;
}
