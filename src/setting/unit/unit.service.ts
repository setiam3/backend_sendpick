import { Injectable } from '@nestjs/common';
import { CreateUnitDto } from './dto/create-unit.dto';
import { UpdateUnitDto } from './dto/update-unit.dto';
import { DeleteManyUnitDto } from './dto/delete-many-unit.dto';
import { FindUnitDto } from './dto/find-unit.dto';
import { PrismaService } from '../../core/prisma/prisma.service';

import {
  PaginateFunction,
  paginator,
} from '../../core/prisma/prisma-pagination';

@Injectable()
export class UnitService {
  constructor(private prisma: PrismaService) {}

  create(CreateUnitDto: CreateUnitDto) {
    return this.prisma.unit.create({
      data: CreateUnitDto,
    });
  }

  findAll(FindUnitDto: FindUnitDto) {
    const paginate: PaginateFunction = paginator({
      perPage: FindUnitDto.per_page,
      page: FindUnitDto.page,
    });
    const params = FindUnitDto;
    return paginate(
      this.prisma.unit,
      {
        where: {
          name: { contains: FindUnitDto.filter_name, mode: 'insensitive' },
          code: { contains: FindUnitDto.filter_code, mode: 'insensitive' },
        },
        orderBy: { created_at: 'desc' },
      },
      {},
      params,
    );
  }

  findOne(id: string) {
    try {
      return this.prisma.unit.findFirstOrThrow({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }

  update(id: string, UpdateUnitDto: UpdateUnitDto) {
    return this.prisma.unit.update({
      where: {
        id,
      },
      data: {
        ...UpdateUnitDto,
      },
    });
  }

  remove(id: string) {
    try {
      return this.prisma.unit.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  removeMany(DeleteManyUnitDto: DeleteManyUnitDto) {
    try {
      return this.prisma.unit.deleteMany({
        where: {
          id: {
            in: DeleteManyUnitDto.unit_ids,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
