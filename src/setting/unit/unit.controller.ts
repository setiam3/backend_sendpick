import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
  UseGuards,
  Put,
} from '@nestjs/common';
import { UnitService } from './unit.service';
import { CreateUnitDto } from './dto/create-unit.dto';
import { UpdateUnitDto } from './dto/update-unit.dto';
import { FindUnitDto } from './dto/find-unit.dto';
import { DeleteManyUnitDto } from './dto/delete-many-unit.dto';
import { ApiTags } from '@nestjs/swagger';
import { AbilitiesGuard } from '../../core/casl/abilities.guard';

@Controller('units')
@ApiTags('Unit')
@UseGuards(AbilitiesGuard)
export class UnitController {
  constructor(private readonly unitService: UnitService) {}

  @Post()
  create(@Body() createUnitDto: CreateUnitDto) {
    return this.unitService.create(createUnitDto);
  }

  @Get()
  findAll(@Query() findAllUnitDto: FindUnitDto) {
    return this.unitService.findAll(findAllUnitDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.unitService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateUnitDto: UpdateUnitDto) {
    return this.unitService.update(id, updateUnitDto);
  }

  @Delete()
  removeMany(@Query() unit_ids: DeleteManyUnitDto) {
    return this.unitService.removeMany(unit_ids);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.unitService.remove(id);
  }
}
