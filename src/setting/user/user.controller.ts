import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiTags } from '@nestjs/swagger';
import { checkAbilites } from '../../core/decorator/abilities.decorator';
import { AbilitiesGuard } from '../../core/casl/abilities.guard';
import { TransformPasswordPipe } from '../../core/auth/transform-password.pipe';

@Controller('user')
@ApiTags('Users')
@UseGuards(AbilitiesGuard)
export class UserController {
  constructor(private readonly userService: UserService) { }

  @checkAbilites({ action: 'create', subject: 'User' })
  @Post()
  @UsePipes(ValidationPipe, TransformPasswordPipe)
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  // @checkAbilites({ action: 'manage', subject: 'User' })
  @Get()
  findAll() {
    return this.userService.findAll();
  }

  @checkAbilites({ action: 'manage', subject: 'User', conditions: true })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(id);
  }

  @Patch(':id')
  @UsePipes(ValidationPipe, TransformPasswordPipe)
  @checkAbilites({ action: 'update', subject: 'User', conditions: true })
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(id, updateUserDto);
  }

  @Delete(':id')
  @checkAbilites({ action: 'delete', subject: 'User', conditions: true })
  remove(@Param('id') id: string) {
    return this.userService.remove(id);
  }

  //upload foto for user
  async uploadAvatar(
    @Param('id') id: string,
    @Body() file: Express.Multer.File,
  ) {
    const checkUserExists = await this.userService.findOne(id);
    if (checkUserExists) {
      const updateAvatar = await this.userService.updateAvatar(id, file);
      if (updateAvatar) {
        return {
          message: 'Success',
          data: updateAvatar,
        };
      }
    }
  }
}
