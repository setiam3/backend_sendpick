import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from '../../core/prisma/prisma.service';
import { faker } from '@faker-js/faker';
@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) { }
  create(createUserDto: CreateUserDto) {
    createUserDto.role_id = 2;
    return this.prisma.user.create({
      data: createUserDto,
    });
  }
  findAll() {
    return this.prisma.user.findMany({
      include: {
        role: {
          select: {
            id: true,
            name: true,
          },
        },
      },
      orderBy: {
        id: 'desc',
      },
    });
  }
  findOne(id: string) {
    try {
      return this.prisma.user.findUniqueOrThrow({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }
  findByEmail(email: string) {
    return this.prisma.user.findFirstOrThrow({
      where: {
        email,
      },
    });
  }
  update(id: string, updateUserDto: UpdateUserDto) {
    return this.prisma.user.update({
      where: {
        id,
      },
      data: {
        ...updateUserDto,
      },
    });
  }
  remove(id: string) {
    try {
      return this.prisma.user.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
  async seedUsers(count: number) {
    for (let i = 0; i < count; i++) {
      await this.prisma.user.create({
        data: {
          email: faker.internet.email(),
          password: faker.internet.password(),
          first_name: faker.internet.userName(),
          role_id: 1,
        },
      });
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  updateAvatar(id: string, file: Express.Multer.File) {
    return this.prisma.user.update({
      where: {
        id,
      },
      data: {
        // avatar: file.filename,
      },
    });
  }
}
