import { Injectable } from '@nestjs/common';
import { CreateAreaDto } from './dto/create-area.dto';
import { UpdateAreaDto } from './dto/update-area.dto';
import { PrismaService } from '../../core/prisma/prisma.service';
import { FindAreaDto } from './dto/find-area.dto';
import {
  PaginateFunction,
  paginator,
} from '../../core/prisma/prisma-pagination';
import { DeleteManyAreaDto } from './dto/delete-many-area.dto';

@Injectable()
export class AreaService {
  constructor(private prisma: PrismaService) {}

  create(CreateAreaDto: CreateAreaDto) {
    return this.prisma.area.create({
      data: CreateAreaDto,
    });
  }

  findAll(FindAreaDto: FindAreaDto) {
    // const areasToSkip = (FindAreaDto.page - 1) * FindAreaDto.per_page;
    const paginate: PaginateFunction = paginator({
      perPage: FindAreaDto.per_page,
      page: FindAreaDto.page,
    });
    const params = FindAreaDto;
    return paginate(
      this.prisma.area,
      {
        where: {
          name: { contains: FindAreaDto.filter_name, mode: 'insensitive' },
        },
        orderBy: { created_at: 'desc' },
      },
      {},
      params,
    );
  }

  findOne(id: string) {
    try {
      return this.prisma.area.findFirstOrThrow({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }

  update(id: string, UpdateAreaDto: UpdateAreaDto) {
    return this.prisma.area.update({
      where: {
        id,
      },
      data: {
        ...UpdateAreaDto,
      },
    });
  }

  remove(id: string) {
    try {
      return this.prisma.area.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  removeMany(DeleteManyAreaDto: DeleteManyAreaDto) {
    try {
      return this.prisma.area.deleteMany({
        where: {
          id: {
            in: DeleteManyAreaDto.area_ids,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
