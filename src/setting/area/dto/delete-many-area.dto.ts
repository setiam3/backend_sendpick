import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class DeleteManyAreaDto {
  @ApiProperty()
  @IsNotEmpty()
  area_ids: Array<string>;
}
