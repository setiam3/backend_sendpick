import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseGuards,
  Query,
  Put,
} from '@nestjs/common';
import { AreaService } from './area.service';
import { CreateAreaDto } from './dto/create-area.dto';
import { UpdateAreaDto } from './dto/update-area.dto';
import { ApiTags } from '@nestjs/swagger';
import { AbilitiesGuard } from '../../core/casl/abilities.guard';
import { FindAreaDto } from './dto/find-area.dto';
import { DeleteManyAreaDto } from './dto/delete-many-area.dto';

@Controller('areas')
@ApiTags('Area')
@UseGuards(AbilitiesGuard)
export class AreaController {
  constructor(private readonly areaService: AreaService) {}

  @Post()
  create(@Body() createAreaDto: CreateAreaDto) {
    return this.areaService.create(createAreaDto);
  }

  @Get()
  findAll(@Query() findAllAreaDto: FindAreaDto) {
    return this.areaService.findAll(findAllAreaDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.areaService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateAreaDto: UpdateAreaDto) {
    return this.areaService.update(id, updateAreaDto);
  }

  @Delete()
  removeMany(@Query() area_ids: DeleteManyAreaDto) {
    return this.areaService.removeMany(area_ids);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.areaService.remove(id);
  }
}
