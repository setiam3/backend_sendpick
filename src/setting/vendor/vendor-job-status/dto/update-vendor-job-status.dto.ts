import { PartialType } from '@nestjs/swagger';
import { CreateVendorJobStatusDto } from './create-vendor-job-status.dto';

export class UpdateVendorJobStatusDto extends PartialType(CreateVendorJobStatusDto) {}
