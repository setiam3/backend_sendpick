import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class DeleteManyVendorJobStatusDto {
  @ApiProperty()
  @IsNotEmpty()
  vendor_ids: Array<string>;
}
