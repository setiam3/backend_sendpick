import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsString, IsNotEmpty, IsNumber, IsBoolean } from 'class-validator';

export class CreateVendorJobStatusDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  slug: string;

  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @IsNotEmpty()
  priority: number;

  @ApiProperty()
  @IsBoolean()
  @Type(() => Boolean)
  @IsNotEmpty()
  editable: boolean;
}
