export class VendorJobStatus {
  id: string;
  name: string;
  slug: string;
  priority: number;
  editable: boolean;
}
