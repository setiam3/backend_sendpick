import { Module } from '@nestjs/common';
import { VendorJobStatusService } from './vendor-job-status.service';
import { VendorJobStatusController } from './vendor-job-status.controller';

@Module({
  controllers: [VendorJobStatusController],
  providers: [VendorJobStatusService],
})
export class VendorJobStatusModule {}
