import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
  UseGuards,
  Put,
} from '@nestjs/common';
import { VendorJobStatusService } from './vendor-job-status.service';
import { CreateVendorJobStatusDto } from './dto/create-vendor-job-status.dto';
import { UpdateVendorJobStatusDto } from './dto/update-vendor-job-status.dto';
import { FindVendorJobStatusDto } from './dto/find-vendor-job-status.dto';
import { DeleteManyVendorJobStatusDto } from './dto/delete-many-vendor-job-status.dto';
import { ApiTags } from '@nestjs/swagger';
import { AbilitiesGuard } from '../../../core/casl/abilities.guard';

@Controller('job-statuses')
@ApiTags('Vendor Job Status')
@UseGuards(AbilitiesGuard)
export class VendorJobStatusController {
  constructor(
    private readonly vendorJobStatusService: VendorJobStatusService,
  ) {}

  @Post()
  create(@Body() createVendorJobStatusDto: CreateVendorJobStatusDto) {
    return this.vendorJobStatusService.create(createVendorJobStatusDto);
  }

  @Get()
  findAll(@Query() findAllVendorJobStatusDto: FindVendorJobStatusDto) {
    return this.vendorJobStatusService.findAll(findAllVendorJobStatusDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.vendorJobStatusService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateVendorJobStatusDto: UpdateVendorJobStatusDto,
  ) {
    return this.vendorJobStatusService.update(id, updateVendorJobStatusDto);
  }

  @Delete()
  removeMany(@Query() vendor_ids: DeleteManyVendorJobStatusDto) {
    return this.vendorJobStatusService.removeMany(vendor_ids);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.vendorJobStatusService.remove(id);
  }
}
