import { Test, TestingModule } from '@nestjs/testing';
import { VendorJobStatusService } from './vendor-job-status.service';

describe('VendorJobStatusService', () => {
  let service: VendorJobStatusService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VendorJobStatusService],
    }).compile();

    service = module.get<VendorJobStatusService>(VendorJobStatusService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
