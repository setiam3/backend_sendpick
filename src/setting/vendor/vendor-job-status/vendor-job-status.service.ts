import { Injectable } from '@nestjs/common';
import { CreateVendorJobStatusDto } from './dto/create-vendor-job-status.dto';
import { UpdateVendorJobStatusDto } from './dto/update-vendor-job-status.dto';
import { DeleteManyVendorJobStatusDto } from './dto/delete-many-vendor-job-status.dto';
import { FindVendorJobStatusDto } from './dto/find-vendor-job-status.dto';
import { PrismaService } from '../../../core/prisma/prisma.service';

import {
  PaginateFunction,
  paginator,
} from '../../../core/prisma/prisma-pagination';

@Injectable()
export class VendorJobStatusService {
  constructor(private prisma: PrismaService) {}

  create(CreateVendorJobStatusDto: CreateVendorJobStatusDto) {
    return this.prisma.vendorJobStatus.create({
      data: CreateVendorJobStatusDto,
    });
  }

  findAll(FindVendorJobStatusDto: FindVendorJobStatusDto) {
    const paginate: PaginateFunction = paginator({
      perPage: FindVendorJobStatusDto.per_page,
      page: FindVendorJobStatusDto.page,
    });
    const params = FindVendorJobStatusDto;
    return paginate(
      this.prisma.vendorJobStatus,
      {
        where: {
          name: {
            contains: FindVendorJobStatusDto.filter_name,
            mode: 'insensitive',
          },
        },
        orderBy: { priority: 'asc' },
      },
      {},
      params,
    );
  }

  findOne(id: string) {
    try {
      return this.prisma.vendorJobStatus.findFirstOrThrow({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }

  update(id: string, UpdateVendorJobStatusDto: UpdateVendorJobStatusDto) {
    return this.prisma.vendorJobStatus.update({
      where: {
        id,
      },
      data: {
        ...UpdateVendorJobStatusDto,
      },
    });
  }

  remove(id: string) {
    try {
      return this.prisma.vendorJobStatus.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  removeMany(DeleteManyVendorJobStatusDto: DeleteManyVendorJobStatusDto) {
    try {
      return this.prisma.vendorJobStatus.deleteMany({
        where: {
          id: {
            in: DeleteManyVendorJobStatusDto.vendor_ids,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
