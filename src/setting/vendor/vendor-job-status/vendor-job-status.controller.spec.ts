import { Test, TestingModule } from '@nestjs/testing';
import { VendorJobStatusController } from './vendor-job-status.controller';
import { VendorJobStatusService } from './vendor-job-status.service';

describe('VendorJobStatusController', () => {
  let controller: VendorJobStatusController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VendorJobStatusController],
      providers: [VendorJobStatusService],
    }).compile();

    controller = module.get<VendorJobStatusController>(VendorJobStatusController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
