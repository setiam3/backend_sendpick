import { Module } from '@nestjs/common';
import { VendorTypeService } from './vendor-type.service';
import { VendorTypeController } from './vendor-type.controller';

@Module({
  controllers: [VendorTypeController],
  providers: [VendorTypeService],
})
export class VendorTypeModule {}
