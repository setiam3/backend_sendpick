import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
  UseGuards,
  Put,
} from '@nestjs/common';
import { VendorTypeService } from './vendor-type.service';
import { CreateVendorTypeDto } from './dto/create-vendor-type.dto';
import { UpdateVendorTypeDto } from './dto/update-vendor-type.dto';
import { FindVendorTypeDto } from './dto/find-vendor-type.dto';
import { DeleteManyVendorTypeDto } from './dto/delete-many-vendor-type.dto';
import { ApiTags } from '@nestjs/swagger';
import { AbilitiesGuard } from '../../../core/casl/abilities.guard';

@Controller('types')
@ApiTags('Vendor Type')
@UseGuards(AbilitiesGuard)
export class VendorTypeController {
  constructor(private readonly vendorTypeService: VendorTypeService) {}

  @Post()
  create(@Body() createVendorTypeDto: CreateVendorTypeDto) {
    return this.vendorTypeService.create(createVendorTypeDto);
  }

  @Get()
  findAll(@Query() findAllVendorTypeDto: FindVendorTypeDto) {
    return this.vendorTypeService.findAll(findAllVendorTypeDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.vendorTypeService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateVendorTypeDto: UpdateVendorTypeDto,
  ) {
    return this.vendorTypeService.update(id, updateVendorTypeDto);
  }

  @Delete()
  removeMany(@Query() vendor_ids: DeleteManyVendorTypeDto) {
    return this.vendorTypeService.removeMany(vendor_ids);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.vendorTypeService.remove(id);
  }
}
