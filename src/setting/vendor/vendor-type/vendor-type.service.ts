import { Injectable } from '@nestjs/common';
import { CreateVendorTypeDto } from './dto/create-vendor-type.dto';
import { UpdateVendorTypeDto } from './dto/update-vendor-type.dto';
import { DeleteManyVendorTypeDto } from './dto/delete-many-vendor-type.dto';
import { FindVendorTypeDto } from './dto/find-vendor-type.dto';
import { PrismaService } from '../../../core/prisma/prisma.service';

import {
  PaginateFunction,
  paginator,
} from '../../../core/prisma/prisma-pagination';

@Injectable()
export class VendorTypeService {
  constructor(private prisma: PrismaService) {}

  create(CreateVendorTypeDto: CreateVendorTypeDto) {
    return this.prisma.vendorType.create({
      data: CreateVendorTypeDto,
    });
  }

  findAll(FindVendorTypeDto: FindVendorTypeDto) {
    const paginate: PaginateFunction = paginator({
      perPage: FindVendorTypeDto.per_page,
      page: FindVendorTypeDto.page,
    });
    const params = FindVendorTypeDto;
    return paginate(
      this.prisma.vendorType,
      {
        where: {
          name: {
            contains: FindVendorTypeDto.filter_name,
            mode: 'insensitive',
          },
        },
        orderBy: { created_at: 'desc' },
      },
      {},
      params,
    );
  }

  findOne(id: string) {
    try {
      return this.prisma.vendorType.findFirstOrThrow({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }

  update(id: string, UpdateVendorTypeDto: UpdateVendorTypeDto) {
    return this.prisma.vendorType.update({
      where: {
        id,
      },
      data: {
        ...UpdateVendorTypeDto,
      },
    });
  }

  remove(id: string) {
    try {
      return this.prisma.vendorType.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  removeMany(DeleteManyVendorTypeDto: DeleteManyVendorTypeDto) {
    try {
      return this.prisma.vendorType.deleteMany({
        where: {
          id: {
            in: DeleteManyVendorTypeDto.vendor_ids,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
