import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class DeleteManyVendorTypeDto {
  @ApiProperty()
  @IsNotEmpty()
  vendor_ids: Array<string>;
}
