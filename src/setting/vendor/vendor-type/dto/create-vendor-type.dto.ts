import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class CreateVendorTypeDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;
}
