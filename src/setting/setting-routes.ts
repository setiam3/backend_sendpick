import { Routes } from '@nestjs/core';
import { SettingModule } from './setting.module';
import { UserModule } from './user/user.module';
import { CityModule } from './city/city.module';
import { MenuModule } from './menu/menu.module';
import { AreaModule } from './area/area.module';
import { CountryModule } from './country/country.module';
import { ProvinceModule } from './province/province.module';
import { UnitModule } from './unit/unit.module';
import { BankModule } from './bank/bank.module';
import { AccountModule } from './finance/account/account.module';
import { AccountTypeModule } from './finance/account-type/account-type.module';
import { VendorJobStatusModule } from './vendor/vendor-job-status/vendor-job-status.module';
import { VendorTypeModule } from './vendor/vendor-type/vendor-type.module';

export const setting: Routes = [
  {
    path: 'setting',
    module: SettingModule,
    children: [
      {
        path: '/',
        module: UserModule,
      },
      {
        path: '/',
        module: CountryModule,
      },
      {
        path: '/',
        module: ProvinceModule,
      },
      {
        path: '/',
        module: CityModule,
      },
      {
        path: '/',
        module: AreaModule,
      },
      {
        path: '/',
        module: UnitModule,
      },
      {
        path: '/',
        module: BankModule,
      },
      {
        path: 'finance/',
        module: AccountTypeModule,
      },
      {
        path: 'finance/',
        module: AccountModule,
      },
      {
        path: 'vendor/',
        module: VendorJobStatusModule,
      },
      {
        path: 'vendor/',
        module: VendorTypeModule,
      },
      {
        path: '/',
        module: MenuModule,
      },
    ],
  },
];
