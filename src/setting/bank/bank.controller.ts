import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
  UseGuards,
  Put,
} from '@nestjs/common';
import { BankService } from './bank.service';
import { CreateBankDto } from './dto/create-bank.dto';
import { UpdateBankDto } from './dto/update-bank.dto';
import { FindBankDto } from './dto/find-bank.dto';
import { DeleteManyBankDto } from './dto/delete-many-bank.dto';
import { ApiTags } from '@nestjs/swagger';
import { AbilitiesGuard } from '../../core/casl/abilities.guard';

@Controller('banks')
@ApiTags('Bank')
@UseGuards(AbilitiesGuard)
export class BankController {
  constructor(private readonly bankService: BankService) {}

  @Post()
  create(@Body() createBankDto: CreateBankDto) {
    return this.bankService.create(createBankDto);
  }

  @Get()
  findAll(@Query() findAllBankDto: FindBankDto) {
    return this.bankService.findAll(findAllBankDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bankService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateBankDto: UpdateBankDto) {
    return this.bankService.update(id, updateBankDto);
  }

  @Delete()
  removeMany(@Query() bank_ids: DeleteManyBankDto) {
    return this.bankService.removeMany(bank_ids);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bankService.remove(id);
  }
}
