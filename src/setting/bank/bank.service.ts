import { Injectable } from '@nestjs/common';
import { CreateBankDto } from './dto/create-bank.dto';
import { UpdateBankDto } from './dto/update-bank.dto';
import { DeleteManyBankDto } from './dto/delete-many-bank.dto';
import { FindBankDto } from './dto/find-bank.dto';
import { PrismaService } from '../../core/prisma/prisma.service';

import {
  PaginateFunction,
  paginator,
} from '../../core/prisma/prisma-pagination';

@Injectable()
export class BankService {
  constructor(private prisma: PrismaService) {}

  create(CreateBankDto: CreateBankDto) {
    return this.prisma.bank.create({
      data: CreateBankDto,
    });
  }

  findAll(FindBankDto: FindBankDto) {
    const paginate: PaginateFunction = paginator({
      perPage: FindBankDto.per_page,
      page: FindBankDto.page,
    });
    const params = FindBankDto;
    return paginate(
      this.prisma.bank,
      {
        where: {
          name: { contains: FindBankDto.filter_name, mode: 'insensitive' },
          code: { contains: FindBankDto.filter_code, mode: 'insensitive' },
        },
        orderBy: { created_at: 'desc' },
      },
      {},
      params,
    );
  }

  findOne(id: string) {
    try {
      return this.prisma.bank.findFirstOrThrow({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }

  update(id: string, UpdateBankDto: UpdateBankDto) {
    return this.prisma.bank.update({
      where: {
        id,
      },
      data: {
        ...UpdateBankDto,
      },
    });
  }

  remove(id: string) {
    try {
      return this.prisma.bank.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  removeMany(DeleteManyBankDto: DeleteManyBankDto) {
    try {
      return this.prisma.bank.deleteMany({
        where: {
          id: {
            in: DeleteManyBankDto.bank_ids,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  }
}
