import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class DeleteManyBankDto {
  @ApiProperty()
  @IsNotEmpty()
  bank_ids: Array<string>;
}
