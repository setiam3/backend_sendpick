import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Query,
  UseGuards,
  Put,
} from '@nestjs/common';
import { AccountService } from './account.service';
import { CreateAccountChildDto } from './dto/create-account-child.dto';
import { CreateAccountParentDto } from './dto/create-account-parent.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { DeleteManyAccountDto } from './dto/delete-many-account.dto';
import { ApiTags } from '@nestjs/swagger';
import { AbilitiesGuard } from '../../../core/casl/abilities.guard';

@Controller('accounts')
@ApiTags('Account')
@UseGuards(AbilitiesGuard)
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Post('parent')
  createParent(@Body() createAccountParentDto: CreateAccountParentDto) {
    return this.accountService.createParent(createAccountParentDto);
  }

  @Post('sub-parent')
  createChild(@Body() createAccountChildDto: CreateAccountChildDto) {
    return this.accountService.createChild(createAccountChildDto);
  }

  @Get()
  findAll() {
    return this.accountService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.accountService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateAccountDto: UpdateAccountDto) {
    return this.accountService.update(id, updateAccountDto);
  }

  @Delete()
  removeMany(@Query() account_ids: DeleteManyAccountDto) {
    return this.accountService.removeMany(account_ids);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.accountService.remove(id);
  }
}
