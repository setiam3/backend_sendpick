import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsString,
  IsNotEmpty,
  IsNumber,
  IsBoolean,
  IsUUID,
} from 'class-validator';

export class CreateAccountChildDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  code: string;

  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  parent_id: string;

  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  account_type_id: string;

  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @IsNotEmpty()
  group: number;

  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @IsNotEmpty()
  variant: number;

  @ApiProperty()
  @IsNumber()
  @Type(() => Number)
  @IsNotEmpty()
  coa_type: number;

  @ApiProperty()
  @IsBoolean()
  @Type(() => Boolean)
  @IsNotEmpty()
  is_parent: boolean;

  @ApiProperty()
  @IsBoolean()
  @Type(() => Boolean)
  @IsNotEmpty()
  is_cash_count: boolean;
}
