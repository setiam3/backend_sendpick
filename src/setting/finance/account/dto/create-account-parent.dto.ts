import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsString,
  IsNotEmpty,
  IsNumber,
  IsBoolean,
  IsUUID,
} from 'class-validator';

export class CreateAccountParentDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  code: string;

  parent_id: string | null;

  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  account_type_id: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  group: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  variant: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  @Type(() => Number)
  coa_type: number;

  is_parent: boolean;

  @ApiProperty()
  @IsBoolean()
  @IsNotEmpty()
  @Type(() => Boolean)
  is_cash_count: boolean;
}
