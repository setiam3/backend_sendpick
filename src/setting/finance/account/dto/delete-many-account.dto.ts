import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class DeleteManyAccountDto {
  @ApiProperty()
  @IsNotEmpty()
  account_ids: Array<string>;
}
