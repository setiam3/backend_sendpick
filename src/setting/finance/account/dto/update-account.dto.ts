import { PartialType } from '@nestjs/swagger';
import { CreateAccountChildDto } from './create-account-child.dto';

export class UpdateAccountDto extends PartialType(CreateAccountChildDto) {}
