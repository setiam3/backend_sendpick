export class Account {
  id: string;
  code: string;
  name: string;
  account_type_id: string | null;
  parent_id: string | null;
  coa_type: number;
  group: number;
  variant: number;
  is_parent: boolean;
  is_cash_count: boolean;
}
