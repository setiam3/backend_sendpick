import { Injectable } from '@nestjs/common';
import { CreateAccountChildDto } from './dto/create-account-child.dto';
import { CreateAccountParentDto } from './dto/create-account-parent.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { DeleteManyAccountDto } from './dto/delete-many-account.dto';
import { PrismaService } from '../../../core/prisma/prisma.service';

@Injectable()
export class AccountService {
  constructor(private prisma: PrismaService) {}

  createChild(CreateAccountChildDto: CreateAccountChildDto) {
    return this.prisma.account.create({
      data: CreateAccountChildDto,
    });
  }

  createParent(CreateAccountParentDto: CreateAccountParentDto) {
    CreateAccountParentDto.parent_id = null;
    CreateAccountParentDto.is_parent = true;
    return this.prisma.account.create({
      data: CreateAccountParentDto,
    });
  }

  async findAll() {
    const data = this.prisma
      .$queryRaw`SELECT id, name, code, parent_id, is_parent FROM accounts`;
    return data.then((result) => {
      return this.tree(result);
    });
  }

  findOne(id: string) {
    try {
      return this.prisma.account.findFirstOrThrow({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error.message);
    }
  }

  update(id: string, UpdateAccountDto: UpdateAccountDto) {
    return this.prisma.account.update({
      where: {
        id,
      },
      data: {
        ...UpdateAccountDto,
      },
    });
  }

  remove(id: string) {
    try {
      return this.prisma.account.delete({
        where: {
          id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  removeMany(DeleteManyAccountDto: DeleteManyAccountDto) {
    try {
      return this.prisma.account.deleteMany({
        where: {
          id: {
            in: DeleteManyAccountDto.account_ids,
          },
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  tree(data, root = 0) {
    const t = {};
    data.forEach((o) => {
      Object.assign((t[o.id] = t[o.id] || {}), o);
      o.parent_id = o.parent_id || root;
      t[o.parent_id] = t[o.parent_id] || {};
      t[o.parent_id].child = t[o.parent_id].child || [];
      t[o.parent_id].child.push(t[o.id]);
    });
    return t[root].child;
  }
}
