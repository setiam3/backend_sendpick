import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../../core/prisma/prisma.service';
import { FindAccountTypeDto } from './dto/find-account-type.dto';
import {
  PaginateFunction,
  paginator,
} from '../../../core/prisma/prisma-pagination';

@Injectable()
export class AccountTypeService {
  constructor(private prisma: PrismaService) {}
  findAll(FindAccountTypeDto: FindAccountTypeDto) {
    const paginate: PaginateFunction = paginator({
      perPage: FindAccountTypeDto.per_page,
      page: FindAccountTypeDto.page,
    });
    const params = FindAccountTypeDto;
    return paginate(
      this.prisma.accountType,
      {
        where: {
          name: {
            contains: FindAccountTypeDto.filter_name,
            mode: 'insensitive',
          },
        },
        orderBy: { created_at: 'desc' },
      },
      {},
      params,
    );
  }
}
