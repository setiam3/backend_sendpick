import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { AccountTypeService } from './account-type.service';
import { FindAccountTypeDto } from './dto/find-account-type.dto';
import { ApiTags } from '@nestjs/swagger';
import { AbilitiesGuard } from '../../../core/casl/abilities.guard';

@Controller('account-types')
@ApiTags('Account Type')
@UseGuards(AbilitiesGuard)
export class AccountTypeController {
  constructor(private readonly accountTypeService: AccountTypeService) {}

  @Get()
  findAll(@Query() findAllAccountTypeDto: FindAccountTypeDto) {
    return this.accountTypeService.findAll(findAllAccountTypeDto);
  }
}
