import {
  ClassSerializerInterceptor,
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ThrottlerModule } from '@nestjs/throttler';
import { AuthModule } from './core/auth/auth.module';
import { AlsModule } from './core/localstorage/als.module';
import { PrismaModule } from './core/prisma/prisma.module';
import { LoggerMiddleware } from './core/middleware/logger/logger.middleware';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { CustomExceptionFilter } from './core/exceptions/filters/custom-exception.filter';
import { WinstonModule } from 'nest-winston';
import { CacheInterceptor, CacheModule } from '@nestjs/cache-manager';
import { CompressionMiddleware } from '@aml360/nestjs-compression';
import { JwtModule } from '@nestjs/jwt';
import { PrismaExceptionFilter } from './core/prisma/prisma-exception.filter';
import { JwtAuthGuard } from './core/auth/jwt-auth.guard';
import { JwtLoggerMiddleware } from './core/middleware/logger/jwt-logger.middleware';
import { AppRoutingModule } from './app-routing.module';
import { SettingModule } from './setting/setting.module';
import { DisableOptionsMiddleware } from './core/middleware/disableoptions.middleware';
import * as redisStore from 'cache-manager-redis-store';

@Module({
  imports: [
    PrismaModule,
    AuthModule,
    AlsModule,
    SettingModule,
    AppRoutingModule,
    ThrottlerModule.forRoot([
      {
        ttl: 60000,
        limit: 10,
      },
    ]),
    WinstonModule.forRoot({}),
    CacheModule.register({
      isGlobal: true,
      // ttl: 10,
      // max: 20,
      store: redisStore,
      host: 'redis',
      port: 6379,
    }),
    JwtModule.register({
      secret: process.env.JWT_KEY,
      signOptions: {
        expiresIn: process.env.JWT_EXPIRED,
      },
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    },
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_FILTER,
      useClass: PrismaExceptionFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: CustomExceptionFilter,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*'); //log for debug
    consumer.apply(JwtLoggerMiddleware).forRoutes('*');
    consumer.apply(CompressionMiddleware).forRoutes('*');
    consumer.apply(DisableOptionsMiddleware).forRoutes('*');
  }
}
