import { INestApplication, Injectable, OnModuleInit } from '@nestjs/common';
import { Prisma, PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
  constructor() {
    super({
      log: [
        {
          emit: 'event',
          level: 'query',
        },
      ],
    });
  }
  async onModuleInit() {
    await this.$connect();
    this.$use(this.categorySoftDeleteMiddleware);
    this.$use(this.categoryFindMiddleware);
    this.$on('query' as never, async (e) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      console.log(`${e.query} ${e.params}`);
    });
  }

  categorySoftDeleteMiddleware: Prisma.Middleware = async (params, next) => {
    if (params.action == 'delete') {
      // Delete queries
      // Change action to an update
      params.action = 'update';
      params.args['data'] = { deleted: true, deleted_at: new Date() };
    }
    if (params.action == 'deleteMany') {
      // Delete many queries
      params.action = 'updateMany';
      if (params.args.data != undefined) {
        params.args.data['deleted'] = true;
        params.args.data['deleted_at'] = new Date();
      } else {
        params.args['data'] = { deleted: true, deleted_at: new Date() };
      }
    }

    return next(params);
  };

  categoryFindMiddleware: Prisma.Middleware = async (params, next) => {
    if (params.action === 'findUnique' || params.action === 'findFirst') {
      // Change to findFirst - you cannot filter
      // by anything except ID / unique with findUnique
      params.action = 'findFirst';
      // Add 'deleted' filter
      // ID filter maintained
      params.args.where['deleted'] = false;
    }
    if (
      params.action === 'findFirstOrThrow' ||
      params.action === 'findUniqueOrThrow'
    ) {
      if (params.args.where) {
        if (params.args.where.deleted == undefined) {
          // Exclude deleted records if they have not been explicitly requested
          params.args.where['deleted'] = false;
        }
      } else {
        params.args['where'] = { deleted: true };
      }
    }
    if (params.action === 'findMany' || params.action === 'count') {
      // Find many queries
      if (params.args.where) {
        if (params.args.where.deleted == undefined) {
          params.args.where['deleted'] = false;
        }
      } else {
        params.args['where'] = { deleted: true };
      }
    }

    return next(params);
  };

  async enableShutdownHooks(app: INestApplication) {
    process.on('beforeExit', async () => {
      await app.close();
    });
  }
}
