import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class DisableOptionsMiddleware implements NestMiddleware {
  use(req, res, next) {
    if (req.method === 'OPTIONS') {
      res.sendStatus(204);
    } else {
      next();
    }
  }
}
