import { Module } from '@nestjs/common';
import { RbacController } from './rbac.controller';
import { PrismaModule } from '../prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  controllers: [RbacController],
  providers: [],
})
export class RbacModule {}
