import { Controller, Get, Patch, Post, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PrismaService } from '../prisma/prisma.service';
import { checkAbilites } from '../decorator/abilities.decorator';
import { AbilitiesGuard } from './abilities.guard';

@Controller('rbac')
@ApiTags('RBAC')
@UseGuards(AbilitiesGuard)
export class RbacController {
  constructor(private prisma: PrismaService) {}

  @Get('roles')
  @checkAbilites({ action: 'read', subject: 'all' })
  getRoles() {
    return ['admin', 'user'];
  }

  @Get('permissions')
  @checkAbilites({ action: 'read', subject: 'all' })
  getPermissions() {
    return ['create', 'read', 'update', 'delete'];
  }

  @Post('role')
  @checkAbilites({ action: 'create', subject: 'all' })
  createRole() {
    return true;
  }

  @Post('permission')
  @checkAbilites({ action: 'create', subject: 'all' })
  createPermission() {
    return true;
  }

  @Patch('role/:id')
  @checkAbilites({ action: 'update', subject: 'all' })
  updateRole() {
    return true;
  }
}
