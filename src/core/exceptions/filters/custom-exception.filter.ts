import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { FastifyReply } from 'fastify/types/reply';

@Catch(HttpException)
export class CustomExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<FastifyReply>();
    const status = exception.getStatus();
    const errorResponse = exception.getResponse() as
      | { message: string; error: string }
      | string;

    if (typeof errorResponse === 'string') {
      response.status(status).send({
        statusCode: status,
        message: errorResponse,
        error: errorResponse,
      });
    } else {
      response.status(status).send({
        ...errorResponse,
        timestamp: new Date().toISOString(),
        statusCode: status,
      });
    }
  }
}
