import { AuthService } from './auth.service';
import {
  Body,
  Request,
  Controller,
  Get,
  HttpCode,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { TransformPasswordPipe } from './transform-password.pipe';
import { ApiTags } from '@nestjs/swagger';
import { AsyncLocalStorage } from 'async_hooks';
import { ExcludeGlobalGuard } from '../decorator/exclude-global.decorator';
import { CurrentUser } from '../decorator/currentuser.decorator';
import { User } from '@prisma/client';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private als: AsyncLocalStorage<any>,
  ) {}

  @ExcludeGlobalGuard()
  @UsePipes(ValidationPipe, TransformPasswordPipe)
  @Post('register')
  async register(@Body() dto: RegisterDto) {
    return await this.authService.register(dto);
  }

  @HttpCode(200)
  @ExcludeGlobalGuard()
  @Post('login')
  async login(@Body() dto: LoginDto) {
    return await this.authService.login(dto);
  }

  @Get('me')
  async me(@CurrentUser() user: User) {
    return await this.authService.me(user.id);
  }

  @Get('profile')
  async profile(@Request() req: any) {
    return {
      message: 'My Profile: ' + req.user.email,
    };
  }
}
