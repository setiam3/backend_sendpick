import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { HashService } from '../service/hash.service';
import { UserModule } from '../../setting/user/user.module';
import { PrismaModule } from '../prisma/prisma.module';
import { UserService } from '../../setting/user/user.service';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { JwtConfig } from './jwt.config';
import { AlsModule } from '../localstorage/als.module';

@Module({
  imports: [
    AlsModule,
    UserModule,
    PrismaModule,
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false,
    }),
    JwtModule.register({
      secret: JwtConfig.user_secret,
      signOptions: {
        expiresIn: JwtConfig.user_expired,
      },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtService, HashService, UserService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
