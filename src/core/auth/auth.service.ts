import { User as UserEntity } from '../../setting/user/entities/user.entity';
import { HashService } from '../service/hash.service';
import { UserService } from '../../setting/user/user.service';
import { PrismaService } from '../prisma/prisma.service';
import { Prisma } from '@prisma/client';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login.dto';
import { omit } from 'lodash';
import { compare } from 'bcrypt';
import { JwtConfig } from './jwt.config';
import { AsyncLocalStorage } from 'async_hooks';
@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly hashService: HashService,
    private readonly userService: UserService,
    private readonly prisma: PrismaService,
    private readonly als: AsyncLocalStorage<any>,
  ) {}
  public async generateToken(user: UserEntity) {
    return {
      access_token: this.jwtService.sign({
        userId: user.id,
      }),
    };
  }
  public async findUser(criteria: Prisma.UserWhereInput) {
    return await this.prisma.user.findFirst({
      where: criteria,
      include: {
        role: {
          select: {
            name: true,
          },
        },
      },
    });
  }
  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.userService.findByEmail(email);
    if (user && user.password === pass) {
      const { ...result } = user;
      return result;
    }
    return null;
  }
  async register(dto: any) {
    dto.role_id = 2;
    const user = await this.prisma.user.findFirst({
      where: {
        email: dto.email,
      },
    });
    if (user) {
      throw new HttpException('User Exists', HttpStatus.BAD_REQUEST);
    }
    try {
      const createUser = await this.prisma.user.create({
        data: dto,
      });
      if (createUser) {
        return {
          status: 200,
          message: 'Register success',
        };
      }
    } catch (error) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
  }
  async login(dto: LoginDto) {
    const user = await this.prisma.user.findFirst({
      where: { email: dto.email },
      include: {
        role: {
          select: {
            name: true,
          },
        },
      },
    });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    const checkPassword = await compare(dto.password, user.password);
    if (!checkPassword) {
      throw new HttpException('Password Incorrect', HttpStatus.UNAUTHORIZED);
    }
    if (checkPassword) {
      await this.als.run({ userId: user.id }, async () => {});
    }
    return await this.generateJwt(
      user.id,
      user.email,
      user.role_id,
      user,
      JwtConfig.user_secret,
      JwtConfig.user_expired,
    );
  }
  async generateJwt(
    userId: any,
    email: string,
    role_id: number,
    user: any,
    secret: any,
    expired = JwtConfig.user_expired,
  ) {
    const accessToken = await this.jwtService.sign(
      {
        userId,
        email,
        name: user.first_name + ' ' + user.last_name,
        role_id,
      },
      {
        expiresIn: expired,
        secret,
      },
    );
    const userData = {
      id: user.id,
      email: user.email,
      first_name: user.first_name,
      last_name: user.last_name,
      fullname: user.first_name + ' ' + user.last_name,
      role_id: user.role_id,
      role: user.role.name,
    };
    return {
      status: 200,
      accessToken: accessToken,
      userData: omit(userData, ['password', 'created_at', 'updated_at']),
    };
  }

  async me(id: number) {
    const findUser = await this.prisma.user.findFirst({
      where: {
        id,
      },
      include: {
        role: {
          select: {
            name: true,
          },
        },
      },
    });

    const userData = {
      id: findUser.id,
      email: findUser.email,
      first_name: findUser.first_name,
      last_name: findUser.last_name,
      fullname: findUser.first_name + ' ' + findUser.last_name,
      role_id: findUser.role_id,
      role: findUser.role.name,
    };
    return {
      status: 200,
      userData: omit(userData, ['password', 'created_at', 'updated_at']),
    };
  }
}
