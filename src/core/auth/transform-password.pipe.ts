import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class TransformPasswordPipe implements PipeTransform {
  async transform(value: any, metadata: ArgumentMetadata) {
    if (metadata.type === 'body') {
      if (value.hasOwnProperty('password')) {
        const hashedPassword = await bcrypt.hash(value.password, 10);
        value.password = hashedPassword;
      }
    }
    return value;
  }
}
