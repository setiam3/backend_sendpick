export const JwtConfig = {
  user_secret: process.env.JWT_KEY,
  user_expired: process.env.JWT_EXPIRED,
};
