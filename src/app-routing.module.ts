import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { setting } from './setting/setting-routes';
const ROUTES = [...setting];

@Module({
  imports: [RouterModule.register(ROUTES)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
