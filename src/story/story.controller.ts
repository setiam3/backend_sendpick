import {
  Patch,
  Body,
  Get,
  Post,
  Param,
  Delete,
  UseGuards,
  Controller,
} from '@nestjs/common';
import { StoryService } from './story.service';
import { CreateStoryDto } from './dto/create-story.dto';
import { UpdateStoryDto } from './dto/update-story.dto';
import { checkAbilites } from '../core/decorator/abilities.decorator';
import { AbilitiesGuard } from '../core/casl/abilities.guard';
import { ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';

@Controller('stories')
@ApiTags('Stories')
export class StoryController {
  constructor(private readonly storyService: StoryService) {}

  @checkAbilites({ action: 'manage', subject: 'all' })
  @UseGuards(AbilitiesGuard)
  @Post()
  create(@Body() createStoryDto: CreateStoryDto) {
    return this.storyService.create(createStoryDto);
  }

  @Get()
  @ApiResponse({ status: 200, description: 'Successful response' })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  findAll() {
    return this.storyService.findAll();
  }

  @checkAbilites({ action: 'read', subject: 'Story' })
  @UseGuards(AbilitiesGuard)
  @Get(':id')
  @ApiParam({
    name: 'id',
    required: true,
    type: Number,
    description: 'Story ID',
  })
  findOne(@Param('id') id: string) {
    return this.storyService.findOne(+id);
  }

  @checkAbilites({ action: 'read', subject: 'Story', conditions: true })
  @UseGuards(AbilitiesGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateStoryDto: UpdateStoryDto) {
    return this.storyService.update(+id, updateStoryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.storyService.remove(+id);
  }
}
