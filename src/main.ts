import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe, BadRequestException } from '@nestjs/common';
import { FormatResponseInterceptor } from './core/common/interceptors/format-response.interceptor';

declare const module: any;
async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    snapshot: true,
  });

  // const app = await NestFactory.create<NestFastifyApplication>(
  //   AppModule,
  //   new FastifyAdapter(),
  // );

  app.useGlobalInterceptors(new FormatResponseInterceptor());

  const config = new DocumentBuilder()
    .setTitle('ALL About SOLOG JS')
    .setDescription('Rest Api of sologjs')
    .setVersion('0.0.1')
    .addTag('solog')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  // TODO: Setup cors
  // const cors = {
  //   origin: ['http://localhost:3000', 'http://localhost', '*'],
  //   methods: 'GET, PATCH, POST, DELETE,OPTIONS',
  //   preflightContinue: false,
  //   optionsSuccessStatus: 204,
  //   credentials: true,
  //   allowedHeaders: ['*'],
  // };
  // app.enableCors(cors);

  app.enableCors();
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors) => {
        const result = errors.map((error) => ({
          property: error.property,
          message: error.constraints[Object.keys(error.constraints)[0]],
        }));
        return new BadRequestException(result);
      },
      stopAtFirstError: true,
    }),
  );
  await app.listen(process.env.PORT);
  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
