import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ExcludeGlobalGuard } from './core/decorator/exclude-global.decorator';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ExcludeGlobalGuard()
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
